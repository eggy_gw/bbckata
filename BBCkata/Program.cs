﻿using System;

// constants #define equivalent
static class Constants
{
    public const int MAX_INPUT = 3999;
    public const int MIN_INPUT = 0;
    // table of pre converted numbers.
    // empty entry to deal with the no tick in that column case.
    public static readonly string[] THOUSANDS = {"","M","MM","MMM"};
    public static readonly string[] HUNDREDS = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
    public static readonly string[] TENS = { "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC" };
    public static readonly string[] UNITS = { "","I","II","III","IV","V","VI","VII","VII","IX" };
}

// interface from kata spec
public interface IRomanNumeralGenerator
{
    String Generate(int number);
}

// implements the RomanNumeralGenerator interface
public class NumberGenerator : IRomanNumeralGenerator
{
    string IRomanNumeralGenerator.Generate(int number)
    {
        String romanNumber = "";
        int convertedNumber =  number;

        // roman numbers work left to right and are basicaly converted decimals
        // so 1000s, 100s, 10s and units.
        // https://www.rapidtables.com/math/symbols/roman_numerals.html
        // shows breakdown of maths and subjective variations

        // Problem breakdown, maths complex and leads to massive if-then-elsif in an itterative loop
        // so break number into seperate parts and convert those.
        int thousands = 0;
        int hundreds = 0;
        int tens = 0;
        int units = 0;
        int remainder = 0;

        if (Validate(number) == false)
        {
            // TODO improve on this.  Should throw an exception 
            // or at least output that input is outside expected ranges
            romanNumber = "";
        }
        else
        {
            thousands = number / 1000;
            remainder = number % 1000;

            hundreds = remainder / 100;
            remainder = remainder % 100;

            tens = remainder / 10;
            remainder = remainder % 10;

            units = remainder;

            // BOUNDS CHECKING so we dont fall over in a big heap
            if ((thousands > Constants.THOUSANDS.Length) || (hundreds > Constants.HUNDREDS.Length) || (units > Constants.UNITS.Length))
            {
                // code never gets here, Validate() allready checks and ensures we are within bounds.
            }

            // We now know the 4 parts of the number 1000s, 100s, 10s, and units.
            // so just lookup the corresponding index in our lookup table
            romanNumber = Constants.THOUSANDS[thousands] + Constants.HUNDREDS[hundreds] + Constants.TENS[tens] + Constants.UNITS[units];
        }
    
        return romanNumber;
    }

    // validate the input is within the expected values
    // lookup tables wont work with bigger numbers.
    public bool Validate(int number)
    {
        bool passedValidation = false;

        if ((number > Constants.MIN_INPUT) && (number <= Constants.MAX_INPUT))
        {
            passedValidation = true;
        }
        else
        {
            passedValidation = false;
        }

        return passedValidation;
    }

    // Helper method to add the "character" to the passed string "repeatnum" times.
    public String AddACharacter( String startString, String character, int repeatnum)
    {
        string expandedString = "";
        expandedString = startString;
        for (int iCount = 0; iCount < repeatnum; iCount ++)
        {
            expandedString += character;
        }

        return expandedString;
    }
}

// Test harness
//:- TODO seperate running a test and reporting a test result.
public class KataTester
{
    private int testsPassed = 0;
    private int testsFailed = 0;

    // Run a single test, compare passed number to expected value
    public bool RunATest(string expectedValue, int number)
    {
        bool testPassed = false;
        String tempGenerated = "";
        NumberGenerator ourGenerator = null;

        ourGenerator = new NumberGenerator();

        tempGenerated = ((IRomanNumeralGenerator)ourGenerator).Generate(number);
        if (String.Equals(expectedValue, tempGenerated))
        {
            testPassed = true;
        }
        else
        {
            testPassed = false;
        }

        // output result to command line with some formatting
        string tempNumber = number.ToString();
        expectedValue = expectedValue.PadRight(12);
        tempNumber = tempNumber.PadRight(4);

        if (testPassed)
        {
            testsPassed++;
            Console.WriteLine(expectedValue + " -> " + number + "\t\t" + " : Pass");
        }
        else
        {
            testsFailed++;
            // show what expected, what we passed and also what we actualy got
            Console.WriteLine(expectedValue + " -> " + number + "\t\t" + " : Fail, converted to :-" + tempGenerated);
        }

        return testPassed;
    }

    public int NumTestsPassed()
    {
        return testsPassed;
    }

    public int NumTestsFailed()
    {
        return testsFailed;
    }

    public void ReportAllTests ()
    {
        int totalTests = testsPassed + testsFailed;
        Console.WriteLine("Total Tests:- " + totalTests + ", Tests Failed:-" + testsFailed + ", Tests Passed:-" + testsPassed);
    }
}

// console program to run the tests
namespace BBCkata
{
    class Program
    {
        // we only ever need one of these,  a big wrapper for all the tests
        static KataTester ourTester = new KataTester();

        static void Main(string[] args)
        {
            Console.WriteLine("BBC Kata Coding Challenge");
            
            // See if we have params on the command line
            if (args.Length>0)
            {
                // If we have some params, covert first to an int
                int firstParam = 0;
                if (Int32.TryParse(args[0],out firstParam))
                {
                    // generate the conversion
                    ConvertAndDisplay(firstParam);
                }
            }
            // Else we have no parameters
            else
            {
                // Run the tests
                RunAllTests();
                ourTester.ReportAllTests();
            }

            // Press a key to exit
            Console.WriteLine("Press A Key To Exit");
            Console.ReadKey();

            // cleanup
            ourTester = null;
        }

        // Use the harness to run all the tests
        static void RunAllTests()
        {
            // run the tests from the kata spec
            ourTester.RunATest("I", 1);
            ourTester.RunATest("V", 5);
            ourTester.RunATest("X", 10);
            ourTester.RunATest("XX", 20);
            ourTester.RunATest("MMMCMXCIX", 3999);

            // add some more tests to cover other cases
            // specificaly look for problems with subtracticve notation.
            
            //I can be placed before V (5) and X (10) to make 4 and 9
            // one less than 5
            ourTester.RunATest("IV", 4);
            // one less than ten
            ourTester.RunATest("IX", 9);

            // X can be placed before L(50) and C(100) to make 40 and 90
            // ten less than five tens
            ourTester.RunATest("XL", 40);
            // ten less than one hundred
            ourTester.RunATest("XC", 90);

            // C can be placed before D(500) and M(1000) to make 400 and 900
            // one hundred less than five hundreds
            ourTester.RunATest("CD", 400);
            // one hundred less than ten hundreds
            ourTester.RunATest("CM", 900);

            // tests from wiki
            // three 10s and one 10 less 1
            ourTester.RunATest("XXXIX",39);
            // two hundreds, a fifty less ten, a five and a one
            ourTester.RunATest("CCXLVI",246);
            // two hundreds, a five and two ones
            ourTester.RunATest("CCVII", 207);
            // a thousand, a fifty and a ten, a five and a one
            ourTester.RunATest("MLXVI", 1066);

            // random tests
            ourTester.RunATest("MCMLXXVII", 1977);
            ourTester.RunATest("MMXVII", 2018);

            // error cases  make sure we dont crash
            // we expect this to fail so thats a pass
            ourTester.RunATest("",4949);
            ourTester.RunATest("",-1);
            ourTester.RunATest("", 0);
        }

        // Converts and Displays a single number
        static void ConvertAndDisplay(int number)
        {
            NumberGenerator ourGenerator = new NumberGenerator();
            Console.WriteLine(number + " : " + ((IRomanNumeralGenerator)ourGenerator).Generate(number));
            ourGenerator = null;
        }
    }
}
